SET( CMAKE_SYSTEM_NAME Generic )

SET( CMAKE_MAKE_PROGRAM gmake-378 )

# these are ADSP specific, so it makes sense to have them separated from CMAKE_SYSTEM_PROCESSOR
SET( ADSP_PROCESSOR "ADSP-BF52x" )
SET( ADSP_PROCESSOR_SILICIUM_REVISION "0.2" )

# specify the cross compiler
SET( CMAKE_C_COMPILER ccblkfn )
SET( CMAKE_CXX_COMPILER ccblkfn -c++ )

SET( CMAKE_ASM_COMPILER easmBLKFN )
