/*********************************************************************//**
    @addtogroup config_params
    @{
    @file l502_defs.h  Файл содержит определения,
        которые используются как программой BF, так и l502api,
        и кроме того видны программам, которые используют l502api
    @date 28.03.2012
    @author Borisov Alexey <borisov@lcard.ru>
 *************************************************************************/
             
#ifndef L502_GLOBAL_DEFS_H_
#define L502_GLOBAL_DEFS_H_


/** Максимальное количество логических каналов в таблице*/
#define L502_LTABLE_MAX_CH_CNT  256
/** Количество диапазон для измерения напряжений */
#define L502_ADC_RANGE_CNT       6

/** Код АЦП, соответствующий максимальному значению шкалы */
#define L502_ADC_SCALE_CODE_MAX     6000000

/** Минимальное значение делителя частоты синхронного вывода */
#define X502_OUT_FREQ_DIV_MIN       2
/** Максимальное значение делителя частоты синхронного вывода */
#define X502_OUT_FREQ_DIV_MAX       1024
/** Значение делителя частоты вывода по умолчанию (которое также всегда
   используется в L502 с версией прошивки ПЛИС ниже 0.5) */
#define X502_OUT_FREQ_DIV_DEFAULT   2

/** Максимальное значение для аппаратного устреднения по логическому каналу */
#define L502_LCH_AVG_SIZE_MAX   128
/** Максимальное значения делителя частоты АЦП */
#define L502_ADC_FREQ_DIV_MAX   (1024*1024)
/** Максимальное значение делителя частоты синхронного цифрового ввода */
#define L502_DIN_FREQ_DIV_MAX   (1024*1024)

/** Максимальное значение межкадровой задержки для АЦП */
#define L502_ADC_INTERFRAME_DELAY_MAX  (0x1FFFFF)


/** Диапазон ЦАП в вольтах */
#define L502_DAC_RANGE  5.

/** Количество каналов ЦАП */
#define L502_DAC_CH_CNT  2

/** Слово в потоке, означающее, что произошло переполнение */
#define L502_STREAM_IN_MSG_OVERFLOW  0x01010000




/** Флаги состояния для синхронного вывода */
typedef enum {
     /** Флаг указывает, что в настоящее время буфер в модуле на передачу пуст */
    X502_OUT_STATUS_FLAG_BUF_IS_EMPTY = 0x01,
    /** Флаг указывает, что было опустошение буфера на вывод с начала старта синхронного
        ввода-вывода или с момента последнего чтения статуса с помощью
        X502_OutGetStatusFlags() (в зависимости от того, что было последним) */
    X502_OUT_STATUS_FLAG_BUF_WAS_EMPTY = 0x02
} t_x502_out_status_flags;

/** @brief Флаги для управления цифровыми выходами.

    Флаги управления цифровыми выходами. Могут быть объединены через логическое
    “ИЛИ” со значениями цифровых выходов при асинхронном выводе с помощью
    L502_AsyncOutDig() или переданы в L502_PrepareData() при синхронном выводе.*/
typedef enum {
    L502_DIGOUT_WORD_DIS_H = 0x00020000, /**< Запрещение (перевод в третье состояние)
                                             старшей половины цифровых выходов */
    L502_DIGOUT_WORD_DIS_L = 0x00010000  /**< Запрещение младшей половины
                                              цифровых выходов */
} t_l502_digout_word_flags;


/** Константы для выбора опорной частоты */
typedef enum {
    L502_REF_FREQ_2000KHZ  = 2000000, /**< Частота 2МГц */
    L502_REF_FREQ_1500KHZ  = 1500000 /**< Частота 1.5МГц */
} t_l502_ref_freq;


/** Диапазоны измерения для канала АЦП */
typedef enum {
    L502_ADC_RANGE_10 = 0, /**< Диапазон +/-10V */
    L502_ADC_RANGE_5  = 1, /**< Диапазон +/-5V */
    L502_ADC_RANGE_2  = 2, /**< Диапазон +/-2V */
    L502_ADC_RANGE_1  = 3, /**< Диапазон +/-1V */
    L502_ADC_RANGE_05 = 4, /**< Диапазон +/-0.5V */
    L502_ADC_RANGE_02 = 5  /**< Диапазон +/-0.2V */
} t_l502_adc_range;

/** Режим измерения для логического канала */
typedef enum {
    L502_LCH_MODE_COMM = 0, /**< Измерение напряжения относительно общей земли */
    L502_LCH_MODE_DIFF = 1, /**< Дифференциальное измерение напряжения */
    L502_LCH_MODE_ZERO = 2  /**< Измерение собственного нуля */
} t_l502_lch_mode;

/** @brief Режимы синхронизации.

    Режимы задания источника частоты синхронизации и признака начала
    синхронного ввода-вывода */
typedef enum {
    L502_SYNC_INTERNAL        = 0, /**< Внутренний сигнал */
    L502_SYNC_EXTERNAL_MASTER = 1, /**< От внешнего мастера по разъему синхронизации */
    L502_SYNC_DI_SYN1_RISE    = 2, /**< По фронту сигнала DI_SYN1 */
    L502_SYNC_DI_SYN2_RISE    = 3, /**< По фронту сигнала DI_SYN2 */
    L502_SYNC_DI_SYN1_FALL    = 6, /**< По спаду сигнала DI_SYN1 */
    L502_SYNC_DI_SYN2_FALL    = 7  /**< По спаду сигнала DI_SYN2 */
} t_l502_sync_mode;


/** Флаги для обозначения синхронных потоков данных */
typedef enum {
    L502_STREAM_ADC  = 0x01, /**< Поток данных от АЦП */
    L502_STREAM_DIN  = 0x02, /**< Поток данных с цифровых входов */
    L502_STREAM_DAC1 = 0x10, /**< Поток данных первого канала ЦАП */
    L502_STREAM_DAC2 = 0x20, /**< Поток данных второго канала ЦАП */
    L502_STREAM_DOUT = 0x40, /**< Поток данных на цифровые выводы */
    /** Объединение всех флагов, обозначающих потоки данных на ввод */
    L502_STREAM_ALL_IN = L502_STREAM_ADC | L502_STREAM_DIN,
    /** Объединение всех флагов, обозначающих потоки данных на вывод */
    L502_STREAM_ALL_OUT = L502_STREAM_DAC1 | L502_STREAM_DAC2 | L502_STREAM_DOUT
} t_l502_streams;

#define L502_STREAM_OUT_WORD_TYPE_DOUT   0x0 /**< Тип передаваемого отсчета - цифровой вывод */
#define L502_STREAM_OUT_WORD_TYPE_DAC1   0x40000000 /**< Тип передаваемого отсчета - Код для 1-го канала ЦАП */
#define L502_STREAM_OUT_WORD_TYPE_DAC2   0x80000000  /**< Тип передаваемого отсчета - Код для 2-го канала ЦАП */


/** Режим работы модуля L502 */
typedef enum {
    L502_MODE_FPGA  = 0, /**< Все потоки данных передаются через ПЛИС минуя
                              сигнальный процессор BlackFin */
    L502_MODE_DSP   = 1, /**< Все потоки данных передаются через сигнальный
                              процессор, который должен быть загружен
                              прошивкой для обработки этих потоков */
    L502_MODE_DEBUG = 2  /**< Отладочный режим */
} t_l502_mode;

/** @brief Номера каналов ЦАП.

    Номер каналов ЦАП для указания в L502_AsyncOutDac() */
typedef enum {
    L502_DAC_CH1 = 0, /**< Первый канал ЦАП */
    L502_DAC_CH2 = 1  /**< Второй канал ЦАП */
} t_l502_dac_ch;


/** Флаги, описывающие модуль */
typedef enum {
    /** Признак наличия ЦАП */
    L502_DEVFLAGS_DAC_PRESENT = 0x0001,
    /** Признак наличия гальваноразвязки */
    L502_DEVFLAGS_GAL_PRESENT = 0x0002,
    /** Признак наличия сигнального процессора */
    L502_DEVFLAGS_BF_PRESENT  = 0x0004,
    /** Признак, что во Flash-памяти присутствует информация о модуле */
    L502_DEVFLAGS_FLASH_DATA_VALID   = 0x00010000,
    /** Признак, что во Flash-памяти присутствуют действительные калибровочные
        коэффициенты АЦП */
    L502_DEVFLAGS_FLASH_ADC_CALIBR_VALID = 0x00020000,
    /** Признак, что во Flash-памяти присутствуют действительные калибровочные
        коэффициенты ЦАП */
    L502_DEVFLAGS_FLASH_DAC_CALIBR_VALID = 0x00040000
} t_l502_devinfo_flags;

#endif

/** @} */
