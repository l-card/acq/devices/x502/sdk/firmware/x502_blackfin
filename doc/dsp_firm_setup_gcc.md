Работа модуля в режиме с сигнальным процессором{#sect_dsp_firm_setup_gcc}
=================================================

Сборка и отладка проекта с использованием компилятора gcc и среды Eclipse {#sect_prj_gcc}
==========================================================
Информацию о использовании открытого программного обеспечения (в частности компилятора GCC) для написания программ для сигнального процессора BlackFin можно получить на сайте http://docs.blackfin.uclinux.org/doku.php. В частности, наибольший интерес представляет раздел [Bare Metal Toolchain](http://docs.blackfin.uclinux.org/doku.php?id=toolchain:bare_metal), описывающий разработку программ под BlackFin без использования операционной системы на сигнальном процессоре.

Сборка проекта {#sect_prj_gcc_build}
------------------------------------------------------------
Для начала необходимо скачать и установить необходимый набор инструментов (toolchain) [со странички проекта на sourceforge]( https://sourceforge.net/projects/adi-toolchain/files/2014R1/2014R1-RC2). Для сборки программ без использования ОС нужен toolchain <b> bfin-elf </b>. После установки необходимо добавить путь к bfin-elf/bin в переменную окружения PATH.

Также необходима утилита make для сборки проекта по файлу makefile. В Linux ее можно установить из репозитория дистрибутива, а для Windows она идет вместе с Toolchain и устанавливается в extra-bin. Соответственно путь к make нужно добавить к путям в переменной окружения PATH.

В исходниках штатной прошивки в директории gcc хранятся все файлы, специфичные именно для сборки с помощью компилятора gcc (скрипт линкера, startup-код и т.д.). В частности в нем хранится makefile, описывающий процедуру сборки проекта.

Для сборки необходимо зайти в директорию gcc и вызвать:
  - для сборки отладочной версии (без оптимизации)
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
  make CONFIG:=debug
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  - для сборки финальной версии (с оптимизацией -O2):
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
  make CONFIG:=release
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  Результаты сборки будут в директории build/debug/bin или build/release/bin соответственно. Файл l502-bf.elf используется для отладки через интерфейс JTAG, а файл l502-bf.ldr для заливки через HostDMA с помощью `L502_BfLoadFirmware()` из l502api.

Отладка через JTAG из среды Eclipse {#sect_prj_gcc_debug}
----------------------------------------------------------------
Для отладки в первую очередь Вам нужен JTAG-эмулятор, который поддерживается открытым инструментарием, так как не все JTAG-эмуляторы от Analog Devices поддерживаются. Список поддерживаемых JTAG-эмуляторов можно посмотреть в разделе [Available JTAG solutions](http://docs.blackfin.uclinux.org/doku.php?id=hw:jtag). Работоспособность отладки была проверена для JTAG-эмулятора [ADI ICE 100B](http://docs.blackfin.uclinux.org/doku.php?id=hw:jtag:ice100b).

В ОС Linux может понадобится разрешить доступ к JTAG-эмулятору не root-пользователю, для чего необходимо добавить правило для udev. Для [ADI ICE 100B](http://docs.blackfin.uclinux.org/doku.php?id=hw:jtag:ice100b) можно скопировать файл gcc/ice-100B.rules в /etc/udev/rules.d (после этого возможно понадобится отключить и подключить снова JTAG-эмулятор). Для других эмуляторов с USB интерфейсом Вам нужно в этом файле изменить параметры idVendor и idProduct на параметры Вашего USB-устройства.

В ОС Windows для отладки через JTAG с использованием открытого ПО Вам понадобится специальный драйвер, который устанавливается вместе с gnu-toolchain, а не тот, что устанавливается вместе с VisualDSP. Для работы с [ADI ICE 100B](http://docs.blackfin.uclinux.org/doku.php?id=hw:jtag:ice100b) Вам понадобится изменить файл gnICE-drivers/gnICE.inf и добавить в нем в секции в секции [Devices.NTX86] и [Devices.NTAMD64] строчку
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
  "ICE-100B" = USB_Install, USB\VID_064b&PID_0225&REV_0100
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
После этого в диспетчере устройств нужно указать явно, что нужно установить драйвер из директории gnICE-drivers.

Следует иметь ввиду, что в прошивке 2.0.0.6 для [ADI ICE 100B](http://docs.blackfin.uclinux.org/doku.php?id=hw:jtag:ice100b) ввели возможность изменить PID USB-устройства с помощью джампера J2. Если он одет, то эмулятор будет иметь PID=0x1225, а если снят --- то  PID=0x0255 (по-умолчанию). Это позволяет использовать данный эмулятор на одном компьютере для отладки как в VisualDSP, так и с использованием GNU Toolchain без постоянной перестановки драйверов. Драйвера VisualDSP устанавливаются всегда на устройство с PID=0x0255 и, если в gnICE-drivers/gnICE.inf вместо указанной до этого строчки добавить
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
  "ICE-100B" = USB_Install, USB\VID_064b&PID_0225&REV_0100
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
то при одетом джампере J2 можно будет отлаживаться через GNU Toolchain, а со снятым --- через VisualDSP (отключая и снова подключая эмулятор по USB).

После того, как JTAG эмулятор подключен, нужно запустить `bfin-gdbproxy` из набора инструментов bfin-elf:
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
  bfin-gdbproxy bfin --reset
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  Если вывод будет подобен приведенному ниже, значит доступ к сигнальному процессору через JTAG успешно получен:
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
  Found USB cable: ICE-100B
  ICE-100B firmware version is 2.0.7
  IR length: 5
  Chain length: 1
  Device Id: 00100010011111100000000011001011 (0x227E00CB)
    Manufacturer: Analog Devices, Inc. (0x0CB)
    Part(0):      BF527 (0x27E0)
    Stepping:     2
    Filename:     /home/user/opt/bfin-elf/bin/../share/urjtag/analog/bf527/bf527
  warning:   bfin: no board selected, BF527 is detected
  notice:    bfin: jc: waiting on TCP port 2001
  notice:    bfin: jc:  (you must connect GDB before using jtag console)
  notice:    bfin-gdbproxy: waiting on TCP port 2000
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Следует отметить, что gdbproxy не различает ADSP-BF523, ADSP-BF525 и ADSP-BF527 и всегда показывает, что видит BF527, как видно из лога выше.

Следующим шагом можно установить среду Eclipse. Работе со средой Eclipse при написании программ для BlackFin посвящена следующая страница http://docs.blackfin.uclinux.org/doku.php?id=toolchain:eclipse.
Вначале необходимо установить саму среду с официального сайта. Она доступна для скачивания в разных вариантах. Для разработки на C/С++ можно скачать соответствующий вариант [Eclipse IDE for C/C++ Developers](http://www.eclipse.org/downloads/packages/eclipse-ide-cc-developers/neonr). 

Дальше можно нужно установить плагин для работы с BlackFin, как описано на странице https://docs.blackfin.uclinux.org/doku.php?id=toolchain:eclipse:install.

Далее добавляем в Eclipse проект прошивки с помощью меню "File->Import", выбираем "General-> Existing Projects into Workspace".
\image latex eclipse_import_prj_type.png "Импорт существующего проекта" width=0.55\textwidth
\image html eclipse_import_prj_type.png "Импорт существующего проекта"

Указываем путь к проекту прошивки и жмем Finish.

\image latex eclipse_import_prj_dir.png "Выбор проекта для импорта" width=0.55\textwidth
\image html eclipse_import_prj_dir.png "Выбор проекта для импорта"

Теперь проект загружен в Eclipse. Открываем представление для редактирования кода (Windows->Open Perspective->Others... C/C++).

\image latex eclipse_c_perspective.png "Импортированный проект прошивке в C/C++ перспективе" width=1\textwidth
\image html eclipse_c_perspective.png "Импортированный проект прошивке в C/C++ перспективе"

Теперь проект можно собирать из Eclipse выбрав проект в Project Explorer и нажав Project->Build Project (Ctrl + B). Следует отметить, что в проекте уже созданы две конфигурации - Debug и Release для сборки соответствующих версий проекта (изменить текущую можно через Project->Build Configurations->Set Active.

Для отладки выбираем Run->Debug Configuration... и в открывшемся окне выбираем "BlackFin GNU Toolchain Bare Metal Configuration (ELF) on JTAG". Должна быть подхвачена конфигурация "l502-bf Debug" с настройками из файла "gcc/l502-bf Debug.launch".

\image latex eclipse_debug_cfg.png "Настройка сеанса отладки" width=0.9\textwidth
\image html eclipse_debug_cfg.png "Настройка сеанса отладки"

Убеждаемся что bfin-gdbproxy запущен (его необходимо всегда держать запущенным при отладке) и жмем Debug, после чего выполнится загрузка прошивки и eclipse предложит перейти в Debug Perspective, в которой можно выполнять отладку.

\image latex eclipse_debug.png "Отладка прошивки l502-bf" width=1\textwidth
\image html eclipse_debug.png "Отладка прошивки l502-bf"
