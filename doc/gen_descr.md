Общее описание устройства модулей{#sect_gen_descr}
==============================================================
Данный раздел предоставляет дополнительные сведения о низкоуровневом устройстве модулей с точки зрения программиста, а не непосредственно аппаратной реализации.

Описание внутреннего устройства платы L502{#sect_gen_descr_l502}
==============================================================

Ниже представлена общая схема L502 с точки зрения взаимодействия различных составных частей модуля L502.

\image latex l502_ll_sch.jpeg "Взаимодействие функциональных блоков модуля L502" width=\textwidth

Как видно из рисунка, модуль состоит из следующих блоков:
1. ПЛИС. В нем реализовано:
    - Вся логика интерфейса PCI-Express
    - Логика доступа к памяти сигнального процессора с ПК через регистры PCI
    - Блок DMA для прямой передачи данных АЦП/DIN в память ПК и чтения данных ЦАП/DOUT из памяти ПК.
    - Блок управления вводом-выводом. В нем реализовано две группы регистров:
        - \f$ $\hyperref[tab:regIoHard]{IO\_HARD} $ \f$  - для настройки параметров сбора данных (логическая таблица, делитель частоты, режим синхронизации и т.д.)
        - \f$ $\hyperref[tab:regIoArith]{IO\_ARITH} $ \f$ - для дополнительной обработки (аппаратное применение калибровочных коэффициентов)
    - Реализованы интерфейсы к Flash-памяти, Сигнальному процессору и гальваноотвязанной части.
2. Flash-память SST25VF16B --- память на 2 Мбайта. Старшая половина пользователю не доступна --- в ней хранится прошивка ПЛИС, ее резервная копия, калибровочные коэффициенты и серийный номер изделия. Младшая половина доступна пользователю для чтения и записи, и может быть использована для каких-либо своих целей. Следует однако учитывать, что Flash-память доступна только для ПК, и к ней нет прямого доступа у сигнального процессора.
3. Сигнальный процессор [ADSP-BF523](http://www.analog.com/en/processors-dsp/blackfin/adsp-bf523/products/product.html). Может быть использован для написания собственных программ обработки данных внутри модуля для расширения возможностей L502.
4. PLDA - ПЛИС, управляющий гальваноотвязанной частью. Именно на гальваноотвязанной части находятся все сигнальные входы и выходы модуля, АЦП и ЦАП. Ими непосредственно управляет PLDA. Сам PLDA управляется через гальваноразвязанный интерфейс основным ПЛИС и логика работы PLDA скрыта от пользователя, т.к. пользователю доступно управление сбором данных только через регистры блока ввода-вывода основного ПЛИС.

Все управление модулем со стороны ПК идет через регистры PCI-Express (с программной стороны аналогичны регистрам PCI) и описано подробно в разделе @ref sect_pc_ctl.

Как видно из рисунка, модуль может работать в двух режимах: штатном (всю обработку выполняет ПЛИС) и DSP-режиме. В зависимости от режима меняются путь, который проходят потоки данных, а также доступ к управляющим регистрам ввода-вывода.

Если в штатном режиме данные, приходящие по гальваноотвязанному интерфейсу (АЦП и DIN), передаются сразу в буфер блока DMA для последующей записи по интерфейсу PCI-Express непосредственно в память ПК, то в DSP-режиме данные передаются через SPORT0 в BlackFin, который может обработать эти данные и, при желании, поставить их на передачу в ПК. В этом случае ПЛИС по HostDMA прочитает данные из памяти BlackFin и передаст их через блок DMA в память ПК.

Аналогично для потока на вывод в режиме DSP данные, прочитанные блоком DMA из памяти ПК, будут записаны через интерфейс HostDMA в память BlackFin, где могут быть обработаны сигнальным процессором и переданы через SPORT0 обратно в ПЛИС для дальнейшего их вывода на ЦАП или цифровые выходы.

Также при переходе в режим DSP изменяется право доступа к регистрам \f$ $\hyperref[tab:regIoHard]{IO\_HARD} $ \f$ и \f$ $\hyperref[tab:regIoArith]{IO\_ARITH} $ \f$. Если в штатном режиме данные регистры доступны для программирования со стороны ПК как обычные регистры PCI, то в режиме DSP управление этими регистрами передается сигнальному процессору, который может через интерфейс SPI записывать и считывать их содержимое. При этом состав регистров и относительные адреса остаются теми же. 


<B> Внимание! </B>: Здесь и далее названия направлений передачи потоков (ввод или вывод) даются относительно ПК. То есть под потоком ввода (IN) понимается поток данных АЦП/DIN, а под потоком вывода (OUT) --- поток данных ЦАП/DOUT, даже когда речь идет о прошивке сигнального процессора.

Описание внутреннего устройства платы E502{#sect_gen_descr_e502}
==============================================================
Ниже представлена общая схема E502 с точки зрения взаимодействия различных составных частей модуля E502.
\image latex e502_ll_sch.jpeg "Взаимодействие функциональных блоков модуля E502" width=\textwidth

Как видно, схема модуля во многом идентична схеме для модуля L502. С функциональной стороны идентично выполнены следующие блоки:
1. Вся гальваноотвязанная часть 
2. Подключение сигнального процессора [ADSP-BF523](http://www.analog.com/en/processors-dsp/blackfin/adsp-bf523/products/product.html)
3. В основном ПЛИС идентично реализованы блоки (включая идентичность всех регистров и их адресов):
    - Блок управления вводом-выводом (регистры \f$ $\hyperref[tab:regIoHard]{IO\_HARD} $ \f$   и \f$ $\hyperref[tab:regIoArith]{IO\_ARITH} $ \f$)
    - Логика доступа к памяти сигнального процессора с ПК через регистры
4. Также присутствует Flash-память SST25VF16B с аналогичным распределением адресного пространства.
5. Модуль также имеет два режима работы (с сигнальным процессором и без него) с аналогичным изменением пути прохождения данных

В связи с вышесказанным модуль E502 полностью идентичен модулю L502 с точки зрения программы сигнального процессора, что позволяет использовать без изменений прошивку от L502 в E502 и наоборот. 

Отличие модулей находится главным образом в интерфейсной части с ПК. Для реализации логики интерфейсов USB и Ethernet (TCP/IP) дополнительно установлен контроллер ARM Cortex M4/M0 [LPC4333](http://www.nxp.com/products/microcontrollers-and-processors/arm-processors/lpc-cortex-m-mcus/lpc-cortex-m4/lpc4300-cortex-m4-m0:MC_1403790133078) (далее ARM- контроллер) с выделенной подключенной памятью SDRAM на 32 МБайта и встроенной flash-памятью для программы (и настроек Ethernet интерфейса).

ARM-контроллер имеет собственную прошивку, которую рекомендуется обновить до [последней доступной версии](https://bitbucket.org/lcard/e502_m4/downloads) с помощью программы [LQMeasStudio](https://bitbucket.org/lcard/lqmeasstudio). При необходимости изменения логики работы устройства с точки зрения интерфейсов (например для создания полностью автономного устройства) пользователь может заказать разработку специализированной прошивки в "Л Кард", либо самостоятельно изменить прошивку. Исходные коды прошивки открыты и их можно скачать с помощью Mercurial из [репозитория проекта](https://bitbucket.org/lcard/e502_m4), однако данный документ не содержит подробного описания работы данной прошивки.

Все взаимодействие с ПК соответственно идет через ARM-контроллер, который может принимать поток данных на ввод, на вывод, а также пользовательские команды.
В связи с этим также в схеме включены следующие изменения по сравнению с L502:
1. Все Регистры ПЛИС, которые в L502 напрямую доступны через память ПК по PCI-Express, в E502 доступны ARM-контроллеру по SPI-интерфейсу
2. Flash-память SST25VF16B подключена напрямую к ARM-контроллеру по SPI без участия ПЛИС. Также на ARM возложена задача загрузки прошивки ПЛИС при старте
3. Вместо блока DMA с набором регистров в ПЛИС для передачи потоков реализованы простые буферы FIFO на ввод и на вывод, которые доступны ARM-контроллеру через интерфейс асинхронной памяти
4. Введен дополнительный светодиод для индикации состояния интерфейса USB и специальных режимов работы контроллера, который подключен напрямую к ножке ARM-контроллера
5. Дополнительно сделан UART0 для отладочных целей или возможности в будущем реализации работы через интерфейс RS-232/RS-485 через внешний преобразователь уровней. Однако этот интерфейс не выведен на корпус и в настоящее время доступен только при снятии корпуса модуля.



